import { combineReducers } from "redux";
import { shoeReducer } from "./shoeReducer";

export const rootReducer_Ex_Demo_Shoe_Redux = combineReducers({
  shoeReducer,
});
