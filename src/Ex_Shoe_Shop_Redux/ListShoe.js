import React, { Component } from "react";
import ItemShoe from "./ItemShoe";
import { connect } from "react-redux";

class ListShoe extends Component {
  render() {
    console.log(this.props.list);
    return (
      <div>
        <div className="row">
          {this.props.list.map((item, index) => {
            return <ItemShoe key={index} shoe={item} />;
          })}
        </div>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    list: state.shoeReducer.listShoe,
  };
};
export default connect(mapStateToProps)(ListShoe);
