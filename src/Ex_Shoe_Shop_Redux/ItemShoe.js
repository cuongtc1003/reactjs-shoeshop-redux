import React, { Component } from "react";
import { connect } from "react-redux";

class ItemShoe extends Component {
  render() {
    let { image, name, price } = this.props.shoe;
    return (
      <div className="col-3">
        <div className="card mb-3">
          <img src={image} className="card-img-top" alt="..." />
          <div className="card-body">
            <h5 className="card-title">{name}</h5>
            <strong>
              <p
                style={{ fontSize: "20px" }}
                className="card-text mt-2 mb-2 text-danger"
              >
                {price}$
              </p>
            </strong>
            <a
              onClick={() => {
                this.props.handlePushToCart(this.props.shoe);
              }}
              style={{ fontWeight: "bold", border: "3px solid black" }}
              href="#"
              className="btn btn-light"
            >
              Add To Cart
            </a>
          </div>
        </div>
      </div>
    );
  }
}
let mapDispatchToProps = (dispatch) => {
  return {
    handlePushToCart: (shoe) => {
      let action = {
        type: "ADD_TO_CART",
        payload: shoe,
      };
      dispatch(action);
    },
  };
};
export default connect(null, mapDispatchToProps)(ItemShoe);
