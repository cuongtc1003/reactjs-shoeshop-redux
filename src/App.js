// import logo from "./logo.svg";
import "./App.css";
import ExShoesRedux from "./Ex_Shoe_Shop_Redux/ExShoesRedux";

function App() {
  return (
    <div className="App">
      <ExShoesRedux />
    </div>
  );
}

export default App;
